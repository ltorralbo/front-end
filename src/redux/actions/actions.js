import axios from 'axios';
import { API_BASE } from '../../env/index';

import {
    GET_HOTELS,
    GET_HOTELS_ERROR
} from './actionTypes';

export const getHotels = () => async (dispatch, getState) => {

  const config = {
    headers: {
      'Content-Type': 'application/json',
    },
  };
  axios
    .get(`${API_BASE}/hotels/`, config)
    .then((res) => {
      dispatch({
        type: GET_HOTELS,
        payload: res.data,
      });
    })
    .catch((error) => {
      dispatch({
        type: GET_HOTELS_ERROR,
        payload: error.response,
      });
    });
};


