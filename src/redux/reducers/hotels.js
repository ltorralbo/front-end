import { GET_HOTELS } from '../actions/actionTypes';

const initialState = {
  hotels: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_HOTELS:
      return {
        ...state,
        hotels: action.payload,
      };
    default:
      return state;
  }
}