import React from 'react'

export default function HotelCard({name, img, openModal}) {
    return (
        <div>
             <div className="card row flex-dir-c flexa-jcsb flexa-ai p10 img imghotel-1 wMin150 hMin150 m2px mt10 bcWhite">
             <h2 className="textsize-1 fs-sbold cWhite">{name}</h2>
             <a href="#" className="btn bcPurple" onClick={openModal}>Reservar</a>
         </div>
        </div>
    )
}
