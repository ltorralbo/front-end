import React, { useEffect, useState }from 'react'
import HotelCard from "../HotelCard/HotelCard";
import { useSelector, useDispatch } from 'react-redux';
import { getHotels } from '../../redux/actions/actions';
import IosClose from 'react-ionicons/lib/IosExitOutline';
import IosSearch from 'react-ionicons/lib/IosExitOutline';




export default function Hotels() {
const [modal, setModal] = useState(false);

const [searching, setSearching] = useState(false);
const [searchText, setSearchText] = useState('');


const handleSearch = () => {
    if (searchText.length) {
      setSearching(!searching);
    }
  };

  const handleResetSearch = () => {
    setSearching(!searching);
    setSearchText('');
  };

  const handleSearchText = (text) => {
    setSearchText(text);
  };

  const handleOpenModal = (post) => {
    setModal(true);
  };

  const handleCloseModal = () => {
    setModal(false);
  };

  const hotels = useSelector((state) => state.hotels.hotels);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getHotels());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


    return (
        <div>
             <div className="flex-display flex-dir-r card p10 w96Porc mt10 bcWhite flexa-jcsb">
                  <div>
                    <div>
                    <input
                        type="text"
                        placeholder="Buscar..."
                        value={searchText}
                        onChange={(e) => handleSearchText(e.target.value)}
                    />
                    {searching === false ? (
                        <IosSearch className="cWhite" fontSize="35px" color="#C9C9C9" />
                    ) : (
                        <IosClose className="cWhite" fontSize="35px" color="#C9C9C9" onClick={handleResetSearch}/>
                    )}
                    </div>
                  </div>     
            </div>
            <h2 className="ml10px mt10 textsize-1 fs-sbold cBlack">Hoteles Disponibles</h2>

            <div className="row flex-dir-r w96Porc flex-wrap ">
            {hotels && hotels
            .filter( hotel => {
                //filter by name 
                return hotel.name.toLowerCase().indexOf(searchText.toLowerCase()) >= 0
            })
            .map(hotel => (
                <HotelCard key={hotel.id} name={hotel.name} img={hotel.imgUrl}  openModal={() => handleOpenModal()}/>
            ))}
            {/* <div className="card row flex-dir-c flexa-jcsb flexa-ai p10 img imghotel-2 wMin150 hMin150 m2px mt10 bcWhite">
                <h2 className="textsize-1 fs-sbold cWhite">WANHARA HOTEL</h2>
                <a href="#" className="btn bcPurple">Reservar</a>
            </div>
            <div className="card row flex-dir-c flexa-jcsb flexa-ai p10 img imghotel-3 wMin150 hMin150 m2px mt10 bcWhite">
                <h2 className="textsize-1 fs-sbold cWhite">WANHARA HOTEL</h2>
                <a href="#" className="btn bcPurple">Reservar</a>
            </div>
            <div className="card row flex-dir-c flexa-jcsb flexa-ai p10 img imghotel-4 wMin150 hMin150 m2px mt10 bcWhite">
                <h2 className="textsize-1 fs-sbold cWhite">WANHARA HOTEL</h2>
                <a href="#" className="btn bcPurple">Reservar</a>
            </div>

            <div className="card row flex-dir-c flexa-jcsb flexa-ai p10 img imghotel-5 wMin150 hMin150 m2px mt10 bcWhite">
                <h2 className="textsize-1 fs-sbold cWhite">WANHARA HOTEL</h2>
                <a href="#" className="btn bcPurple">Reservar</a>
            </div> */}
        </div>
        {modal && (
        <div>
         <IosClose className="cWhite" fontSize="35px" color="#C9C9C9" beat={true} onClick={handleCloseModal}/>
          <HotelCard />
        </div>
      )}
        </div>
    )
}
