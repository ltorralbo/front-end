import React, {useState} from 'react'
import IosClose from 'react-ionicons/lib/IosExitOutline';
import IosSearch from 'react-ionicons/lib/IosExitOutline';



export default function SearchBar() {

    const [searching, setSearching] = useState(false);
    const [searchText, setSearchText] = useState('');
   

    const handleSearch = () => {
        if (searchText.length) {
          setSearching(!searching);
        }
      };

      const handleResetSearch = () => {
        setSearching(!searching);
        setSearchText('');
      };
    
      const handleSearchText = (text) => {
        setSearchText(text);
      };
  
    return (
        <div className="flex-display flex-dir-r card p10 w96Porc mt10 bcWhite flexa-jcsb">
                  <div>
                    <div>
                    <input
                        type="text"
                        placeholder="Buscar..."
                        value={searchText}
                        onChange={(e) => handleSearchText(e.target.value)}
                    />
                    {searching === false ? (
                        <IosSearch className="cWhite" fontSize="35px" color="#C9C9C9" beat={true} />
                    ) : (
                        <IosClose className="cWhite" fontSize="35px" color="#C9C9C9" beat={true} onClick={handleResetSearch}/>
                    )}
                    </div>
                    <button type="button" onClick={handleSearch}>
                            Buscar
                    </button>
                  </div>     
        </div>
       
    )
}
