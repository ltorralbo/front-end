import React from 'react';
import IosAnalyticsOutline from 'react-ionicons/lib/IosAnalyticsOutline'
import IosMailOutline from 'react-ionicons/lib/IosMailOutline'
import IosCard from 'react-ionicons/lib/IosCard'
import IosAppsOutline from 'react-ionicons/lib/IosAppsOutline'
import IosExitOutline from 'react-ionicons/lib/IosExitOutline'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Dashboard from "./components/Dashboard/Dashboard";
import Header from "./components/Header/Header";
import Hotels from "./components/Hotels/Hotels";

import { Provider } from 'react-redux';
import store from './redux/store/store';


const App = () => {
  
  return (
    <Provider store={store}>
    <Router>
            <div className="container">
            <Header/>
            <section className="wMax1000 panel flex-one pl12em pr12em">
            <Dashboard/>
            <div className="containerCenter">
                <div className="row flex-dir-c">
                <Switch>
                <Route exact path="/" component={Hotels} />
                </Switch>
                </div>
            </div>

        </section>
        
      </div>   

      </Router>  
      </Provider>

  );
}

export default App;
